import { Component, Input, OnInit, Output,EventEmitter, ChangeDetectorRef } from '@angular/core';

import { Libro } from '../domain/Libro';

@Component({
  selector: 'app-libro',
  templateUrl: './libro.component.html',
  styleUrls: ['./libro.component.css']
})
export class LibroComponent implements OnInit {
  @Input() libro: Libro;
  
  @Output('delLibro') delLibro= new EventEmitter<Libro>(); // alias
  // tslint:disable-next-line:no-output-rename
  @Output('editLibro') editLibro= new EventEmitter<Libro>(); // alias
  edit : boolean = false;
  
  // iniettare un oggetto nella classe: Dependecy Injection DI
  constructor(private cd: ChangeDetectorRef) { }


  ngOnInit(): void {
  }
  OnDelLibro(libro: Libro){
    console.log("log da sottocomponente",libro);
    this.delLibro.emit(libro);

  }
  
  OnEditLibro(libro: Libro): void{
    console.log(libro);
    this.editLibro.emit(libro);
    this.edit = false;
  }


  OnAbilitaEditing(titolo): void{
    this.edit = !this.edit;
    this.cd.detectChanges(); // aspetta che l'attivazione dell'elemento html input
    titolo.focus();
  }
}
