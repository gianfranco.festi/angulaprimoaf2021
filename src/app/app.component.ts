import { Component } from '@angular/core';
import { Libro } from './domain/Libro';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'primo';
  colore = 'red';
  libri: Libro[] = [
    {id : '1', titolo: 'Angular', autore: 'google', genere: 'web tech'},
    {id : '2', titolo: 'React', autore: 'facebook', genere: 'web tech'}
  ];

  onClick(){
    console.log(this.title);
  }
  OnDelLibro(libro: Libro){
    console.log('log da componente padre', libro);
    // crea un nuovo array
    this.libri = this.libri.filter( libroInElenco => libro.titolo !== libroInElenco.titolo  );
  }

  OnEditLibro(libro: Libro){

    this.libri = this.libri.map(oldLibro =>
      { console.log(oldLibro);
        return oldLibro.id === libro.id ? libro : oldLibro;
      });
    console.log(libro, this.libri);
  }
}
