export interface Libro{
    id    : string;
    titolo: string;
    autore: string;
    genere: string;
}
