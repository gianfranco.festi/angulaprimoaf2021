import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { LibroComponent } from './libro/libro.component';
import { AppHighLightDirective } from './high-light.directive';

@NgModule({
  declarations: [
    AppComponent,
    LibroComponent,
    AppHighLightDirective
  ],
  imports: [
    BrowserModule, FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
