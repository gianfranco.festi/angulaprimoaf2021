import { Directive, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appHighLight]'
})
export class AppHighLightDirective {
  @Input('appHighLight') colore: string;
  @HostBinding('style.backgroundColor') coloredisfondo: string;
  constructor() { }

  @HostListener('mouseenter') mouseover(eventData : Event){
    this.coloredisfondo = this.colore;
    console.log(this.coloredisfondo);
  }
  @HostListener('mouseleave') mouseleave(eventData : Event){
    this.coloredisfondo = "";
    console.log(this.coloredisfondo);
  }
}
